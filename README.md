# Service

This is a library providing an API for developing services for a Java application that communicate that communicate through sockets with a client

## Running tests
When you run unit tests with Intellij you have to delegate the execution to maven.
This can be done by clicking "File" -> "Settings" -> "Build, Execution, Deployments" -> "Build Tools" -> "Maven" -> "Runner" and check "Delegate IDE build/run actions to maven". 