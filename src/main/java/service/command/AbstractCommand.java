package service.command;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class AbstractCommand extends HttpServlet {
    protected void setAccessControlAllowOriginHeader(HttpServletRequest req, HttpServletResponse resp){
        String origin = req.getHeader("Origin");
        if (origin != null){
            resp.setHeader("Access-Control-Allow-Origin", origin);
        }
    }

    protected void displayError(HttpServletResponse resp, int statusCode, String message) throws IOException {
        resp.setStatus(statusCode);
        PrintWriter writer = new PrintWriter(resp.getOutputStream());
        writer.print(message);
        writer.flush();
    }
}
