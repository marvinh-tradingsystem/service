module service {
    requires org.slf4j;
    requires javax.servlet.api;
    requires org.eclipse.jetty.servlet;

    exports service;
    exports service.command;
    exports service.exception;
}