package service;

public abstract class Service {
    public static boolean isRunning(){
        return false;
    }

    public static void start(){

    }

    public static void stop(){

    }

    public static void restart(){
        stop();
        start();
    }
}
